# Win_host

## 一、功能

该程序在Windows端实现接收文件或特定格式消息的功能，接收端口为**5000**。需与如下程序配合使用：

https://gitlab.com/zx-bit/linux_host

## 二、环境

> windows10
>
> visual studio 2017
>
> protobuf 3.17.4

## 三、使用方法

#### 一般方法：

使用visualstudio2017打开该项目，运行前需要在vs中进行库和include的环境配置。

将项目文件夹加入include环境。

将protobuf文件夹的libprotobufd.lib加入lib库链接的环境。

即可运行。

#### 快速方法：

项目的/Debug/win_host.exe为编译后的可执行文件，直接启动即可。

## 四、文件介绍

在protobuf文件夹中，mymsg.proto定义了两端信息交互使用的消息格式，若修改了mymsg.proto需要，运行/protobuf/build.bat，并将生成的.h和.cpp文件替换了/test中的对应文件。

test文件夹中存储的是程序源文件。
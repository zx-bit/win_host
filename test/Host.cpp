#include"Winhost.cpp"
#include<thread>
#define RECEIVE_PORT 5000
#define SEND_PORT 6000
#define DST_IP "192.168.59.130"
using namespace std;

int main()
{
	Winhost host;
	thread([&host] { host.receive_data(RECEIVE_PORT); }).detach();
	//输入文件名以发送 
	string s;
	while (1)
	{
		printf("file or message?\n");
		cin >> s;
		if (s == "file") {
			host.send_data(DST_IP, SEND_PORT, TYPE_FILE);
		}
		else if (s == "message") {
			host.send_data(DST_IP, SEND_PORT, TYPE_MESSAGE);
		}
		else {
			cout << "Input 'file' or 'message'!" << endl;
		}
	}
	return 0;
}
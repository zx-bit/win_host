#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"ws2_32.lib")
#include"mymsg.pb.h"
#include<Windows.h>
#include<cstdio>
#include<string>
#include<iostream>
#define LENGTH_OF_LISTEN_QUEUE 20
#define BUFFER_SIZE 512
#define TYPE_FILE 0
#define TYPE_MESSAGE 1
#define MAXSTRBUF 100000

class Winhost {
public:

	Winhost() {
		// 初始化Winsock服务
		if (WSAStartup(MAKEWORD(2, 2), &s) != 0)
		{
			printf("Init Windows Socket Failed! Error: %d\n", GetLastError());
		}
		if ((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
		{
			printf("Create Socket Failed! Error: %d\n", GetLastError());
		}
	}

	~Winhost() {
		closesocket(sockfd);
		WSACleanup();
	}

	//发送数据
	void send_data(std::string dst_ip, int dst_port, int data_type = TYPE_FILE) {
		SOCKET ClientSocket = socket(AF_INET, // 只支持ARPA Internet地址格式
			SOCK_STREAM, // 新套接口的类型描述
			IPPROTO_TCP); // 套接口所用的协议
		if (ClientSocket == INVALID_SOCKET)
		{
			printf("Create Socket Failed! Error: %d\n", GetLastError());
			return;
		}

		struct sockaddr_in ClientAddr;
		ClientAddr.sin_family = AF_INET;
		ClientAddr.sin_addr.s_addr = inet_addr(dst_ip.c_str());
		ClientAddr.sin_port = htons(dst_port); // 将主机的无符号短整形数转换成网络字节顺序
		memset(ClientAddr.sin_zero, 0X00, 8);

		//socket与对方地址连接 
		if ((connect(ClientSocket, (struct sockaddr*)&ClientAddr, sizeof(ClientAddr))) == SOCKET_ERROR) {
			printf("Socket Connect Failed! Error:%d\n", GetLastError());
			return;
		}
		else {
			printf("Socket Connect Succeed!\n");
		}
		//主任务部分
		if (data_type == TYPE_FILE) {
			send_file(ClientSocket);
		}
		else {
			send_message(ClientSocket);
		}
		//关闭socket
		closesocket(ClientSocket);
	}
	//接收部分，传入参数为接收端口 
	void receive_data(int port) {
		struct sockaddr_in ServerAddr;
		ServerAddr.sin_family = AF_INET;
		ServerAddr.sin_addr.s_addr = INADDR_ANY;
		ServerAddr.sin_port = htons(port); // 将主机的无符号短整形数转换成网络字节顺序
		memset(ServerAddr.sin_zero, 0X00, 8);

		if (bind(sockfd, (struct sockaddr*)&ServerAddr, sizeof(ServerAddr)) == SOCKET_ERROR)
		{
			printf("Server Bind Port: %d Failed!\n", port);
			return;
		}

		if (listen(sockfd, LENGTH_OF_LISTEN_QUEUE))
		{
			printf("Server Listen Failed!\n");
			return;
		}
		else {
			printf("Listening...\n");
		}

		char buffer[BUFFER_SIZE];
		while (1)
		{
			memset(buffer, 0, sizeof(buffer));
			struct sockaddr_in caddr;
			SOCKET csocket;
			int length = sizeof(caddr);

			csocket = accept(sockfd, (struct sockaddr*)&caddr, &length);
			if (csocket < 0)
			{
				printf("Server Accept Failed!\n");
				break;
			}
			//recv the filename and create it
			length = recv(csocket, buffer, BUFFER_SIZE, 0);
			buffer[length] = '\0';
			printf("The receive head is [%s]\n", buffer);

			if (strcmp(buffer,"MSG")==0) {
				printf("Begin to receive a message\n");
				char strbuf[MAXSTRBUF];
				memset(strbuf, 0, sizeof(strbuf));
				int p = 0;
				while ((length = recv(csocket, strbuf + p, BUFFER_SIZE, 0)) > 0)
				{
					p += length;
				}
				std::string str_msg(strbuf, p);
				mymsg msg;
				msg.ParseFromString(str_msg);
				printf("ID: %d\n", msg.id());
				std::cout << "Name: " << msg.name() << std::endl;
				std::cout << "Score: " << msg.score() << std::endl;
				//TODO: process msg received

			}
			else if (strcmp(buffer,"NONE")==0){
				printf("Receive nothing.\n");
				//do nothing
			}
			else {
				printf("Filename is %s\n", buffer);
				FILE* f = fopen(buffer, "wb");
				//循环读取文件并发送数据 
				while ((length = recv(csocket, buffer, BUFFER_SIZE, 0)) > 0)
				{
					fwrite(buffer, 1, length, f);
				}
				fclose(f);
			}
			printf("Receive finish.\n");
			closesocket(csocket);
		}
	}
private:
	WSADATA s; // 用来储存调用AfxSocketInit全局函数返回的Windows Sockets初始化信息
	SOCKET sockfd;

	void send_file(SOCKET& csocket) {
		printf("Input File Name: ");
		std::string filename;
		std::cin >> filename;

		FILE* f = fopen(filename.c_str(), "rb");
		if (f == NULL)
		{
			std::string tip = "NONE";
			send(csocket, tip.c_str(), (int)tip.length(), 0);
			printf("File open error.\n");
			return;
		}
		//发送文件名 
		send(csocket, filename.c_str(), (int)filename.length(), 0);
		//发送文件体 
		int nCount, sum = 0;
		char sendData[BUFFER_SIZE];

		while ((nCount = fread(sendData, 1, BUFFER_SIZE, f)) > 0)
		{
			if (sum % (1024 * 1024) == 0) {
				printf("%dMB\n", sum / (1024 * 1024));
			}
			sum += nCount;
			send(csocket, sendData, nCount, 0);
		}

		fclose(f);
		printf("Send File Success\n");
	}

	void send_message(SOCKET& csocket) {
		std::string name;
		int id;
		float score;
		printf("Input the name:");
		std::cin >> name;
		printf("Input the id:");
		std::cin >> id;
		printf("input the score:");
		std::cin >> score;

		mymsg msg;
		msg.set_name(name);
		msg.set_id(id);
		msg.set_score(score);
		std::string str_msg;
		msg.SerializeToString(&str_msg);

		std::string head = "MSG";
		send(csocket, head.c_str(), (int)head.length(), 0);
		int p = 0;
		while (p < str_msg.length()) {
			if (str_msg.length() - p >= BUFFER_SIZE) {
				send(csocket, str_msg.c_str() + p, BUFFER_SIZE, 0);
				p += BUFFER_SIZE;
			}
			else {
				send(csocket, str_msg.c_str() + p, str_msg.length() - p, 0);
				p = str_msg.length();
			}
		}
		printf("Send Message Success.\n");
		return;
	}
};